const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Search = require('../../models/Search');
const request = require("request");

const iTunesSearchURL = 'https://itunes.apple.com';

let searchRouter = require('express').Router();

router.get('/', function(req, res){
    res.render('index')
});

searchRouter.route('/search')
    .get(function(req, res) {
        req.query.query.split(' ').forEach((word) => {
            let search = new Search();
            search.query = word;
            search.save(function (err) {
                if (err) {
                    console.log('Failed to log search query to database')
                }
            });
        });

        let queryObject = {term: req.query.query};
        request.get({uri: iTunesSearchURL+'/search', qs: queryObject},
            (error, response, body) => {

            return res.send(body)
        });
    });

searchRouter.route('/search/:id')
    .get(function(req, res) {
        let queryObject = {id: req.params.id};
        request.get({uri: iTunesSearchURL+'/lookup', qs: queryObject},
            (error, response, body) => {
                return res.send(body)
            });
    });

router.route('/statistics')
    .get((req, res) => {

        Search.aggregate([
            { $group: {"_id":"$query" , "count":{$sum:1}} },
                { $sort : { count: -1} }, {$limit: 10}],
            (err, result) => {
                return res.send(JSON.stringify(result));
        })

    });

module.exports = {
    router: router,
    searchRouter: searchRouter
};