import React, { Component } from 'react';
import '../../css/navbar/Navbar.css';
import NavBarLink from './NavbarLink'

class Navbar extends Component {

    render() {
        const links = [];

        this.props.links.forEach((link) => {
            links.push(
                <NavBarLink link={link}/>
            )
        });

        return (
          <div className="Navbar">
              {links}
          </div>
        );
    }
}

export default Navbar;
