import React, { Component } from 'react';
import '../../css/navbar/NavbarLink.css';
import {Link} from 'react-router-dom';

class NavbarLink extends Component {

    render() {
        const link = this.props.link;

        return (
            <Link to={link.ref}>{link.name}</Link>
        );
    }
}

export default NavbarLink;
