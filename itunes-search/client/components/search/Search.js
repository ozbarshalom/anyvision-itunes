import React, { Component } from 'react';
import SearchForm from './SearchForm';
import SearchResults from './SearchResults';

class Search extends Component {
    constructor(props) {
        super(props);
        this.updateResults = this.updateResults.bind(this);
        this.state = {
            results: []
        };
    }

    updateResults(results) {
        this.setState(state => ({
            results: results
        }));

        }


    render() {

        return (
            <div>
                <h1>What are you looking for?</h1>
                <SearchForm updateResults={this.updateResults}/>
                <SearchResults  results={this.state.results}/>
            </div>
        );
    }
}

export default Search;
