import React, { Component } from 'react';
import { DefaultPlayer as VideoPlayer } from 'react-html5video';
import 'react-html5video/dist/styles.css';


class Video extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videoUrl: this.props.url
        }
    }

    render() {
        return <VideoPlayer autoPlay loop controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}>
            <source src={this.state.videoUrl} type="video/webm" />
            <track label="English" kind="subtitles" srcLang="en" src="http://source.vtt" default />
        </VideoPlayer>
    }
}

export default Video;
