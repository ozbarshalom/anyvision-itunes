import React, { Component } from 'react';
import '../../css/search/SearchResult.css';
import {Table} from 'react-bootstrap'
import Video from './Video'

const axios = require('axios');

class SearchResultView extends Component {

    constructor() {
        super();

        this.state = {
            item: null
        }
    }

    componentWillMount() {
        axios.get("/search/"+this.props.match.params.searchid).then(response => {
            if (response.data.results.length) {
                this.setState(state => ({
                    item: response.data.results[0]
                }));
            }
        });
    }

    render() {
        if (this.state.item) {
            let videoComponent = <div></div>;
            if (this.state.item.previewUrl) {
                console.log(this.state.item.previewUrl)
                videoComponent = <Video url={this.state.item.previewUrl}/>
            }
            return (
                <div className='item-view'>
                    <Table>
                        <tr>
                            <td className='text-left'><b>Artist:</b></td>
                            <td className='text-left'>{this.state.item.artistName}</td>
                        </tr>
                        <tr>
                            <td className='text-left'><b>Collection Name:</b></td>
                            <td className='text-left'>{this.state.item.collectionName}</td>
                        </tr>
                    </Table>
                    {videoComponent}
                </div>
            );
        }
        else {
            return <h3>Loading...</h3>
        }

    }
}

export default SearchResultView;
