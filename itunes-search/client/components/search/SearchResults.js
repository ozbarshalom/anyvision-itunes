import React, { Component } from 'react';
import SearchResult from './SearchResult';
import {Table} from 'react-bootstrap'

class SearchResults extends Component {

    constructor (props){
        super(props);
        this.state = {results: []};
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState(state => ({
                results: nextProps.results
            }));
        }
    }

    render() {
        return (
            <Table responsive>
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Artist</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    {this.state.results.map((result) => {
                        return <SearchResult result={result}/>;
                    })}
                </tbody>
            </Table>
        );
    }
}

export default SearchResults;
