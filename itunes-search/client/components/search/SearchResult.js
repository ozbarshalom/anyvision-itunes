import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../../css/search/SearchResult.css';

class SearchResult extends Component {

    constructor (props){
        super(props);
        this.state = {
            result: props.result
        };
    }

    render() {
        return (
            <tr>
                <td className='text-left'>{this.state.result.kind}</td>
                <td className='text-left'>{this.state.result.artistName}</td>
                <td className='text-left'>
                    <Link className='resultLink' to={'/results/'+this.state.result.trackId}>{this.state.result.trackName}</Link>
                </td>
            </tr>
        );
    }
}

export default SearchResult;
