import React, { Component } from 'react';
import {FormGroup, FormControl, HelpBlock, InputGroup, Button} from "react-bootstrap";
import '../../css/search/SearchForm.css'

const axios = require('axios');

class SearchForm extends Component {
    constructor(props, context) {
        super(props, context);

        this.startSearch = this.startSearch.bind(this);

    }

    startSearch(e) {
        e.preventDefault();
        const updateResults = this.props.updateResults;
        axios.get('/search?query='+this.searchTerm.value)
            .then(function(response) {
                updateResults(response.data.results)
            });
    }

    render() {

        return (
            <form onSubmit={this.startSearch}>
                <FormGroup controlId="searchForm">
                    <div className="mainQuery">
                        <InputGroup>
                            <FormControl type="text" inputRef={(ref) => {this.searchTerm = ref}}/>
                            <InputGroup.Button>
                                <Button
                                    type="submit">
                                    Search
                                </Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </div>
                    <FormControl.Feedback />
                    <HelpBlock>
                        apps, iBooks, movies, podcasts, music, music videos, audiobooks, and TV shows
                    </HelpBlock>
                </FormGroup>
            </form>
        );
    }
}

export default SearchForm;
