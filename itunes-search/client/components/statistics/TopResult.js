import React, { Component } from 'react';

class TopResult extends Component {

    render() {
        const result = this.props.result;

        return (
            <span>
                {result._id}: {result.count}
            </span>
        );
    }
}

export default TopResult;
