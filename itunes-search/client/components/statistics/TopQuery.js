import React, { Component } from 'react';
import TopResult from './TopResult'

const axios = require('axios');

class TopQuery extends Component {
    constructor() {
        super();

        this.state = {
            results: null
        }
    }

    componentWillMount() {
        let results = [];
        axios.get("/statistics").then(response => {
            response.data.forEach((result, i) => {
                results.push(
                    <div>#{i+1} <TopResult result={result}/></div>
                )

            });
            this.setState(state => ({
                results: results
            }));
        });




    }

    render() {
        return (
            <div>
                <h1>Top 10 queries</h1>
                <h3 className='text-left'>{ this.state.results }</h3>
            </div>
        );
    }
}

export default TopQuery;
