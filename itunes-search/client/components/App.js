import React, { Component } from 'react';
import '../css/App.css';
import logo from '../images/itunes_logo.png'
import Navbar from '../components/navbar/Navbar'
import Container from '../components/Container'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Search from '../components/search/Search'
import TopQuery from '../components/statistics/TopQuery'
import SearchResultView from '../components/search/SearchResultView'

class App extends Component {
    render() {
        const title = 'iTunes Search';
        const links = [
            {name: 'search', ref: '/'},
            {name: 'Top 10 Queries', ref: '/top10'}
        ];

        return (
            <div className="App">
              <BrowserRouter>
                <div>
                      <header className="App-header">
                          <img src={logo} className="App-logo" alt="logo" />
                          <h1 className="App-title">Welcome to {title}</h1>
                          <Navbar links={links}/>
                      </header>
                      <Container>
                          <Switch>
                              <Route exact path='/' component={Search}/>
                              <Route exact path='/top10' component={TopQuery}/>
                              <Route path='/results/:searchid' exact component={SearchResultView}/>
                          </Switch>
                      </Container>
                </div>
              </BrowserRouter>
          </div>
        );
    }
}

export default App;
