const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const searchSchema = new Schema({
    query: String,
    time: Date
});

searchSchema.pre('save', function(next){
    let now = new Date();
    if ( !this.searched_at ) {
        this.searched_at = now;
    }
    next();
});

module.exports = mongoose.model('Search', searchSchema);